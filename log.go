package log

import (
	"log"
	"net/http"
	"os"
	"github.com/luci/go-render/render"
)

var __LOG *log.Logger

func New() {
	__LOG = log.New(os.Stderr, "", log.Lshortfile)
}

func LOG(tolog string) {
	__LOG.Output(2, "LOG: "+tolog)
}

func ERR(e error) {
	__LOG.Output(2, "ERR: "+e.Error())
}

func LOGHIT(req *http.Request) {
	__LOG.Output(2, "URL: "+req.RequestURI)
}

func DUMP(v interface{}) {
	__LOG.Output(2, "DUMP:" + render.Render(v))
}

func PRINTVAL(s string, v interface{}) {
	LOG(s)
	DUMP(v)
}
